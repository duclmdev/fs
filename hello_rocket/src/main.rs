#[macro_use]
extern crate rocket;

use rocket::fs::FileServer;
use std::path::PathBuf;

#[rocket::main]
async fn main() -> Result<(), rocket::Error> {
    println!("Starting...");
    let _rocket = rocket::build() //
        .mount("/", FileServer::from(concat!(env!("CARGO_MANIFEST_DIR"), "/web")))
        .mount("/fs", routes![read_path, upload_path, move_path, delete_path])
        .mount("/ws", routes![echo_stream, echo_compose])
        .launch() //
        .await?;

    Ok(())
}

#[get("/<drive>/<path..>")]
async fn read_path(drive: &str, path: PathBuf) -> String {
    // NamedFile::open(Path::new("static/").join(file)).await.ok()
    format!("get path {}:/{}", drive, path.to_string_lossy())
}

#[post("/<drive>/<path..>")]
async fn upload_path(drive: &str, path: PathBuf) -> String {
    format!("upload path {}:/{}", drive, path.display())
}

#[put("/<drive>/<path..>")]
async fn move_path(drive: &str, path: PathBuf) -> String {
    format!("move path {}:/{}", drive, path.display())
}

#[delete("/<drive>/<path..>")]
async fn delete_path(drive: &str, path: PathBuf) -> String {
    format!("remove path {}:/{}", drive, path.display())
}

#[get("/")]
fn echo_stream(ws: ws::WebSocket) -> ws::Stream!['static] {
    ws::Stream! { ws =>
        for await message in ws {
            println!("{:?}", message.as_ref().unwrap());
            yield message?;
        }
    }
}

#[get("/echo?compose")]
fn echo_compose(ws: ws::WebSocket) -> ws::Stream!['static] {
    ws.stream(|io| io)
}
