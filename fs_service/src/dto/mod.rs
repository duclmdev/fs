use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct ResponseDTO<T> {
    pub code: i32,

    pub message: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub body: Option<T>,
}

impl<T> ResponseDTO<T> {
    pub fn success(body: T) -> Self {
        ResponseDTO {
            code: 0,
            message: String::from("Success"),
            body: Some(body),
        }
    }

    pub fn error(message: String, body: T) -> Self {
        ResponseDTO {
            code: 1,
            message,
            body: Some(body),
        }
    }

    pub fn error_empty(message: String) -> Self {
        ResponseDTO {
            code: 1,
            message,
            body: None,
        }
    }
}
