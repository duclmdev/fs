use std::{ffi::OsStr, io::Result, path::PathBuf};

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum Path {
    Folder { name: String, size: u64 },
    File { name: String, size: u64 },
    Shortcut { name: String, link: String },
}

#[cfg(windows)]
pub fn from_root(root: &String) -> std::path::PathBuf {
    std::path::Path::new(format!("{root}:/").as_str()).to_owned()
}

#[cfg(unix)]
pub fn from_root(root: &String) -> std::path::PathBuf {
    std::path::Path::new(format!("/{root}").as_str()).to_owned()
}

pub fn read_dir<P: std::convert::AsRef<std::path::Path>>(path: P) -> Result<Vec<Path>> {
    Ok(std::fs::read_dir(path)?
        .filter_map(|entry| entry.ok())
        .filter_map(|entry| to_path(entry.path()))
        .collect::<Vec<Path>>())
}

fn to_path(path: PathBuf) -> Option<Path> {
    let metadata = path.metadata();
    let name = path.file_name().unwrap().to_str().unwrap().to_string();
    match metadata {
        Ok(md) if md.is_file() => Some(Path::File { name, size: md.len() }),
        Ok(md) if md.is_dir() => Some(Path::Folder { name, size: md.len() }),
        Ok(md) if md.is_symlink() => Some(Path::Shortcut {
            name,
            link: String::from("test"),
        }),
        _ => None,
    }
}

pub fn to_duration(time: Result<std::time::SystemTime>) -> u128 {
    match time.unwrap().elapsed() {
        Ok(duration) => duration,
        Err(err) => {
            println!("err {}", err);
            err.duration()
        },
    }
    .as_millis()
}

pub fn readable_size(size: u64, si: bool) -> String {
    let power: f64 = if si { (1 << 10) as f64 } else { 1e3 };
    let s = size as f64;
    if s < power {
        return format!("{}B", s);
    }
    let name = ["", "K", "M", "G", "T", "P"];
    let mut i = 0;
    let mut p = 1.0;
    while i < name.len() {
        if s > p * power {
            p *= power;
            i += 1;
            continue;
        }
        break;
    }
    let s = s / power;
    if si {
        format!("{:.2}{}iB", s, name[i])
    } else {
        format!("{:.2}{}B", s, name[i])
    }
}

pub fn content_type(addr: &PathBuf) -> Option<&str> {
    match addr.extension().and_then(OsStr::to_str) {
        Some(ext) => Some(ext_type(ext)),
        None => None,
    }
}

fn ext_type(ext: &str) -> &str {
    match ext {
        "7z" => "application/x-7z-compressed",
        "avi" => "video/x-msvideo",
        "avif" => "image/avif",
        "azw" => "application/vnd.amazon.ebook",
        "bmp" => "image/bmp",
        "bz" => "application/x-bzip",
        "bz2" => "application/x-bzip2",
        "css" => "text/css",
        "csv" => "text/csv",
        "doc" => "application/msword",
        "docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "eot" => "application/vnd.ms-fontobject",
        "epub" => "application/epub+zip",
        "gif" => "image/gif",
        "gz" => "application/gzip",
        "htm" | "html" => "text/html",
        "ico" => "image/vnd.microsoft.icon",
        "ics" => "text/calendar",
        "jar" => "application/java-archive",
        "jpeg" | "jpg" => "image/jpeg",
        "js" | "mjs" => "text/javascript",
        "json" => "application/json",
        "mp3" => "audio/mpeg",
        "mp4" => "video/mp4",
        "mpeg" => "video/mpeg",
        "odp" => "application/vnd.oasis.opendocument.presentation",
        "ods" => "application/vnd.oasis.opendocument.spreadsheet",
        "odt" => "application/vnd.oasis.opendocument.text",
        "otf" => "font/otf",
        "pdf" => "application/pdf",
        "png" => "image/png",
        "ppt" => "application/vnd.ms-powerpoint",
        "pptx" => "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "rar" => "application/vnd.rar",
        "rtf" => "application/rtf",
        "sh" => "application/x-sh",
        "svg" => "image/svg+xml",
        "tar" => "application/x-tar",
        "tif" | "tiff" => "image/tiff",
        "ttf" => "font/ttf",
        "txt" | "ts" => "text/plain",
        "vsd" => "application/vnd.visio",
        "wav" => "audio/wav",
        "weba" => "audio/webm",
        "webm" => "video/webm",
        "webp" => "image/webp",
        "woff" => "font/woff",
        "woff2" => "font/woff2",
        "xhtml" => "application/xhtml+xml",
        "xls" => "application/vnd.ms-excel",
        "xlsx" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "xml" => "application/xml",
        "zip" => "application/zip",
        _ => "application/octet-stream",
    }
}
