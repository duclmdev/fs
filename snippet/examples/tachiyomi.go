package main

import (
	"fmt"
	"os"

	"google.golang.org/protobuf/proto"

	"github.com/duclmse/hello_go/pb"
)

func main() {
	content, err := os.ReadFile("./tachiyomi_2022-12-07_20-45.proto")
	if err != nil {
		fmt.Printf("err %s", err)
		return
	}
	var backup pb.Backup
	err = proto.Unmarshal(content, &backup)
	if err != nil {
		fmt.Printf("err %s", err)
		return
	}
	mangas := backup.BackupManga
	sources := backup.BackupSources
	fmt.Printf("=================================================\n")
	fmt.Printf("sources %v\n", len(sources))
	for i, v := range sources {
		name := v.Name
		if len(name) > 20 {
			name = v.Name[:20]
		}
		fmt.Printf("%3d %20d %20s %20d\n", i, v.SourceId, name, v.SourceId)
	}
	fmt.Printf("=================================================\n")
	fmt.Printf("manga %v\n", len(mangas))
	for i, v := range mangas {
		title := v.Title
		if len(title) > 40 {
			title = v.Title[:40]
		}
		fmt.Printf("%3d %-40v %v\n", i, title, v.Artist)
	}
}
