use std::io::Cursor;

use prost::Message;

pub mod tachiyomi {
    tonic::include_proto!("tachiyomi");
}

// pub fn create_hello_request(name: String) -> hello_world::HelloRequest {
//     let mut hello_request = hello_world::HelloRequest::default();
//     hello_request.name = name;
//     hello_request
// }

// pub fn serialize_greeter(hello: &hello_world::HelloRequest) -> Vec<u8> {
//     let mut buf = Vec::new();
//     buf.reserve(hello.encoded_len());

//     hello.encode(&mut buf).unwrap();
//     buf
// }

// pub fn deserialize_greeter(buf: &[u8]) -> Result<hello_world::HelloRequest, prost::DecodeError> {
//     hello_world::HelloRequest::decode(&mut Cursor::new(buf))
// }

fn main() {
    // let request = String::from("Hello, World!");

    // let greeter_request = create_hello_request(request);
    // let request_vector = serialize_greeter(&greeter_request);
    let backup = match std::fs::read("/Users/duclm/projects/resource/tachiyomi_2024-01-18_02-05.tachibk") {
        Ok(bin) => bin,
        Err(e) => {
            if e.kind() == std::io::ErrorKind::PermissionDenied {
                eprintln!("please run again with appropriate permissions.");
                return;
            }
            panic!("{}: {}", e.kind(), e);
        },
    };
    match tachiyomi::Backup::decode(&mut Cursor::new(&backup)) {
        Ok(result) => println!("okie {:?}", result),
        Err(err) => println!("err {:#?}", err),
    }
}
