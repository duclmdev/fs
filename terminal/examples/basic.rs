// #[cfg(windows)]
fn main() -> std::io::Result<()> {
    use std::io::Read;
    let mut proc = terminal::conpty::spawn("echo Hello World")?;
    let mut reader = proc.output()?;

    println!("Process has pid={}", proc.pid());

    let mut buf = Vec::<u8>::new();
    reader.read_to_end(&mut buf)?;

    assert!(String::from_utf8_lossy(&buf).contains("Hello World"));

    Ok(())
}
