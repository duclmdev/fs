#[cfg(windows)]
fn main() {
    use std::io::prelude::*;
    let console = terminal::conpty::console::Console::current().unwrap();

    assert!(console.is_stdin_empty().unwrap());

    console.set_raw().unwrap();

    println!("Type `]` character to exit\r");

    let mut buf = [0; 1];
    loop {
        let n = std::io::stdin().read(&mut buf).unwrap();
        if n == 0 {
            break;
        }

        assert!(!console.is_stdin_empty().unwrap());

        let c: char = buf[0].into();
        println!("char={}\r", c);

        if c == ']' {
            break;
        }
    }

    console.reset().unwrap();
}
