#[cfg(windows)]
fn main() {
    panic!("An example doesn't supported on windows")
}

#[cfg(unix)]
fn main() -> Result<()> {
    use std::io::Result;
    use terminal::repl::ReplSession;

    #[cfg(not(feature = "async"))]
    {
        let mut p = terminal::spawn("sh")?;
        p.get_process_mut().set_echo(true, None)?;

        let mut shell = ReplSession::new(p, String::from("sh-5.1$"), Some(String::from("exit")), true);

        shell.expect_prompt()?;

        let output = exec(&mut shell, "echo Hello World")?;
        println!("{:?}", output);

        let output = exec(&mut shell, "echo '2 + 3' | bc")?;
        println!("{:?}", output);

        Ok(())
    }

    #[cfg(not(feature = "async"))]
    {
        let buf = shell.execute(cmd)?;
        let mut string = String::from_utf8_lossy(&buf).into_owned();
        string = string.replace("\r\n\u{1b}[?2004l\r", "");
        string = string.replace("\r\n\u{1b}[?2004h", "");

        Ok(string)
    }

    #[cfg(feature = "async")]
    {
        futures_lite::future::block_on(async {
            let mut p = terminal::spawn("sh")?;
            p.get_process_mut().set_echo(true, None)?;

            let mut shell = ReplSession::new(p, String::from("sh-5.1$"), Some(String::from("exit")), true);

            shell.expect_prompt().await?;

            let output = exec(&mut shell, "echo Hello World").await?;
            println!("{:?}", output);

            let output = exec(&mut shell, "echo '2 + 3' | bc").await?;
            println!("{:?}", output);

            Ok(())
        })
    }

    #[cfg(feature = "async")]
    {
        let buf = shell.execute(cmd).await?;
        let mut string = String::from_utf8_lossy(&buf).into_owned();
        string = string.replace("\r\n\u{1b}[?2004l\r", "");
        string = string.replace("\r\n\u{1b}[?2004h", "");

        Ok(string)
    }
}
