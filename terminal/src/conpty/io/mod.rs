//! This module contains [crate::Process]'s `Input` and `Output` pipes.
//!
//! Input - PipeWriter
//! Output - PipeReader

pub(crate) mod reader;
pub(crate) mod writer;

pub use reader::PipeReader;
pub use writer::PipeWriter;
