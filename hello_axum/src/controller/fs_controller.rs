use std::collections::HashMap;

use axum::{
    extract::{Multipart, Path, Query, Request},
    response::{IntoResponse, Response},
    routing::get,
};
use axum_extra::{headers::Range, TypedHeader};

pub(crate) fn get_router() -> axum::Router<()> {
    axum::Router::new() //
        .route("/:root", get(read_root))
        .route(
            "/:root/*path",
            get(read_path)
                // .head(read_path)
                .post(upload_path)
                .patch(upload_file)
                .put(copy_path)
                .delete(delete_path),
        )
}

/// GET
async fn read_root(
    Path(root): Path<String>,
    Query(params): Query<HashMap<String, String>>,
    range: Option<TypedHeader<Range>>,
) -> Response {
    println!("root='{}'", root);
    let addr = fs::service::from_root(&root);
    let info = crate::service::fs::read_path(&addr, &params, &range).await;
    match info {
        Ok(response) => response,
        Err(err) => super::handle_error(err),
    }
}

async fn read_path(
    Path((root, path)): Path<(String, String)>,
    Query(params): Query<HashMap<String, String>>,
    range: Option<TypedHeader<Range>>,
) -> Response {
    println!("root='{}', path='{}'", root, path);
    let addr = fs::service::from_root(&root).join(path);
    let info = crate::service::fs::read_path(&addr, &params, &range).await;
    match info {
        Ok(response) => response,
        Err(err) => super::handle_error(err),
    }
}

/// POST
/// upload new file or overwrite file (do not change uploading file name)
async fn upload_path(
    Path((root, path)): Path<(String, String)>,
    // multipart: Multipart,
    request: Request,
) -> impl IntoResponse {
    let addr = fs::service::from_root(&root).join(&path);
    let upload = crate::service::fs::upload_path(&addr, request).await;
    super::handle_response(upload)
    // format!("{}", addr.display())
}

async fn upload_file(Path((root, path)): Path<(String, String)>, mut multipart: Multipart) -> impl IntoResponse {
    let addr = fs::service::from_root(&root).join(&path);
    while let Some(field) = multipart.next_field().await.unwrap() {
        let name = field.name().unwrap().to_string();
        let file_name = field.file_name().unwrap();
        // let data = field.into_stream();
        // let stream_to_file = stream_to_file(&addr, data).await;
        println!("[{}] saving `{}` to {}", name, &file_name, addr.display());
    }
    // super::handle_response(upload)
    format!("{}", addr.display())
}

/// PUT
/// move/rename file/folder
async fn copy_path(
    Path((root, path)): Path<(String, String)>,
    Query(params): Query<HashMap<String, String>>,
) -> impl IntoResponse {
    let addr = fs::service::from_root(&root).join(path);
    let moved = crate::service::fs::move_path(&addr).await;
    super::handle_response(moved)
}

/// DELETE
/// delete file/folder
async fn delete_path(Path((root, path)): Path<(String, String)>) -> impl IntoResponse {
    let addr = fs::service::from_root(&root).join(path);
    let deleted = crate::service::fs::delete_path(&addr).await;
    super::handle_response(deleted)
}
