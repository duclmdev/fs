//#region import
use axum::{
    body::Bytes,
    error_handling::HandleErrorLayer,
    extract::{DefaultBodyLimit, Path, Query, Request, State},
    handler::Handler,
    http::{HeaderMap, StatusCode},
    response::IntoResponse,
    routing::{get, post},
    Json, Router,
};
use serde::Serialize;

use std::{
    borrow::Cow,
    collections::HashMap,
    sync::{Arc, RwLock},
    time::Duration,
};
use tower::{load_shed::error::Overloaded, timeout::error::Elapsed, BoxError, ServiceBuilder};
use tower_http::{
    compression::CompressionLayer, limit::RequestBodyLimitLayer, trace::TraceLayer,
    validate_request::ValidateRequestHeaderLayer,
};
//#endregion import

pub(crate) fn get_router() -> Router<()> {
    let shared_state = SharedState::default();
    Router::new()
        .route("/path/:user_id", post(path))
        .route("/query", get(query))
        .route("/string", post(string))
        .route("/bytes", post(bytes))
        .route("/headers", post(headers))
        // .route("/json", post(test_json))
        .route("/request", post(request))
        .route("/accept", get(heterogeneous_handle))
        // .route("/extension", post(extension))
        // Nest our admin routes under `/admin`
        .nest("/admin", admin_routes(&shared_state))
        // Add middleware to all routes
        .layer(
            ServiceBuilder::new()
                .layer(HandleErrorLayer::new(handle_error))
                .load_shed()
                .concurrency_limit(1024)
                .timeout(Duration::from_secs(10))
                .layer(TraceLayer::new_for_http()),
        )
        .with_state(Arc::clone(&shared_state))
}

// /test/path/:user_id
async fn path(Path(user_id): Path<u32>) -> String {
    format!("received {}", user_id)
}

// /test/query
// `Query` gives you the query parameters and deserializes them.
async fn query(Query(params): Query<HashMap<String, String>>) -> impl IntoResponse {
    println!("query params {:#?}", params);
    Json(params.clone())
}

// `HeaderMap` gives you all the headers
async fn headers(headers: HeaderMap) -> impl IntoResponse {
    println!("headers {:#?}", headers);
    let map = headers
        .iter()
        .map(|(key, value)| (key.to_string(), String::from(value.to_str().unwrap())))
        .collect::<HashMap<String, String>>();
    Json(map)
}

// `String` consumes the request body and ensures it is valid utf-8
async fn string(body: String) -> String {
    println!("body {}", body);
    body
}

// `Bytes` gives you the raw request body
async fn bytes(body: Bytes) {
    println!("{:#?}", body);
}

// We've already seen `Json` for parsing the request body as json
async fn test_json(Json(payload): Json<Value>) -> impl IntoResponse {
    println!("{:#?}", payload);
    (StatusCode::ACCEPTED, String::from("okie dokie"))
}

// `Request` gives you the whole request for maximum control
async fn request(request: Request) {
    println!("{:#?}", request);
}

// // `Extension` extracts data from "request extensions"
// // This is commonly used to share state with handlers
// async fn extension(State(mut state): State<Arc<AppState>>) -> impl IntoResponse {
//     // state.count += 1;
//     println!("{:?}", state);
//     Json(Arc::get_mut(&mut state).unwrap().to_owned())
// }

#[derive(Clone, Debug)]
pub struct Value {
    pub value: String,
}

type SharedState = Arc<RwLock<AppState>>;

#[derive(Default)]
struct AppState {
    db: HashMap<String, Bytes>,
}

async fn kv_get(Path(key): Path<String>, State(state): State<SharedState>) -> impl IntoResponse {
    let db = &state.read().unwrap().db;

    if let Some(value) = db.get(&key) {
        Ok(value.clone())
    } else {
        Err(StatusCode::NOT_FOUND)
    }
}

async fn kv_set(Path(key): Path<String>, State(state): State<SharedState>, bytes: Bytes) -> impl IntoResponse {
    state.write().unwrap().db.insert(key, bytes);
    read_state(state)
}

async fn list_keys(State(state): State<SharedState>) -> impl IntoResponse {
    read_state(state)
}

fn read_state(state: SharedState) -> impl IntoResponse {
    state
        .read()
        .unwrap()
        .db
        .iter()
        .map(|(key, value)| format!("{}: {:?}", key.to_string(), value))
        .collect::<Vec<String>>()
        .join("\n")
}

fn admin_routes(shared_state: &SharedState) -> Router<SharedState> {
    async fn delete_all_keys(State(state): State<SharedState>) {
        state.write().unwrap().db.clear();
    }

    async fn remove_key(Path(key): Path<String>, State(state): State<SharedState>) {
        state.write().unwrap().db.remove(&key);
    }

    Router::new()
        .route("/keys", get(list_keys).delete(delete_all_keys))
        .route(
            "/:key",
            get(kv_get.layer(CompressionLayer::new())) //
                .post_service(
                    kv_set
                        .layer((DefaultBodyLimit::disable(), RequestBodyLimitLayer::new(1024 * 5_000)))
                        .with_state(Arc::clone(&shared_state)),
                )
                .delete(remove_key),
        )
        .layer(ValidateRequestHeaderLayer::bearer("secret-token"))
}

#[derive(Serialize)]
struct Hello {
    name: String,
}

async fn heterogeneous_handle(headers: HeaderMap) -> axum::response::Response {
    match headers.get(axum::http::header::ACCEPT).map(|x| x.as_bytes()) {
        Some(b"text/plain") => String::from("Hello, world!").into_response(),
        Some(b"application/json") => {
            let hello = Hello {
                name: String::from("world"),
            };
            Json(hello).into_response()
        },
        _ => (StatusCode::BAD_REQUEST, "Please provide accept header").into_response(),
    }
}

async fn handle_error(error: BoxError) -> impl IntoResponse {
    return match error {
        _ if error.is::<Elapsed>() => (StatusCode::REQUEST_TIMEOUT, Cow::from("request timed out")),
        _ if error.is::<Overloaded>() => {
            (StatusCode::SERVICE_UNAVAILABLE, Cow::from("service is overloaded, try again later"))
        },
        _ => (StatusCode::INTERNAL_SERVER_ERROR, Cow::from(format!("Unhandled internal error: {error}"))),
    };
}
