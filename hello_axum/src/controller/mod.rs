use std::{borrow::Cow, io::Error, time::Duration};

use axum::{
    error_handling::HandleErrorLayer,
    http::StatusCode,
    response::{IntoResponse, Response},
    routing::get,
    Json, Router,
};
use serde::Serialize;
use tokio::time::error::Elapsed;
use tower::{load_shed::error::Overloaded, BoxError, ServiceBuilder};
use tower_http::trace::TraceLayer;

use crate::model::ResponseDTO;

mod fs_controller;
#[cfg(debug_assertions)]
mod test_controller;

pub fn get_router() -> Router<()> {
    let app = Router::new() //
        .route("/", get(root))
        .nest("/fs", fs_controller::get_router());

    #[cfg(debug_assertions)]
    let app = app.nest("/test", test_controller::get_router());

    return app.layer(
        ServiceBuilder::new()
            .layer(HandleErrorLayer::new(handle_error_layer))
            .load_shed()
            .concurrency_limit(1024)
            .timeout(Duration::from_secs(10))
            .layer(TraceLayer::new_for_http()),
    );
}

// basic handler that responds with a static string
async fn root() -> &'static str {
    "Hello!"
}

fn handle_response<T: Serialize>(response: Result<T, Error>) -> Response {
    match response {
        Ok(res) => handle_success(res),
        Err(error) => handle_error(error),
    }
}

fn handle_success<T: Serialize>(e: T) -> Response {
    json(ResponseDTO::success(e))
}

fn handle_error(e: Error) -> Response {
    json(ResponseDTO::<()>::error_empty(e.to_string()))
}

fn json<T: serde::Serialize>(e: T) -> Response {
    Json(e).into_response()
}

async fn handle_error_layer(error: BoxError) -> impl IntoResponse {
    match error {
        _ if error.is::<Elapsed>() => (StatusCode::REQUEST_TIMEOUT, Cow::from("request timed out")),
        _ if error.is::<Overloaded>() => (StatusCode::SERVICE_UNAVAILABLE, Cow::from("service is overloaded")),
        _ => (StatusCode::INTERNAL_SERVER_ERROR, Cow::from(format!("Unhandled internal error: {error}"))),
    }
}
