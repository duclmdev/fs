//#region import
use std::{
    collections::HashMap,
    io::{Error, ErrorKind, Result},
    path::PathBuf,
};

use axum::{
    extract::{Multipart, Path, Request},
    response::{IntoResponse, Response},
    Json,
};
use axum_extra::{
    headers::{ContentType, Range},
    TypedHeader,
};
use futures::{Stream, TryStreamExt};
use tokio::{fs::File, io::BufWriter};
use tokio_util::io::StreamReader;

use crate::{
    model::ResponseDTO,
    range::{KnownSize, Ranged},
};
//#endregion import

//#region read path
pub(crate) async fn read_path(
    addr: &PathBuf,
    params: &HashMap<String, String>,
    range: &Option<TypedHeader<Range>>,
) -> Result<Response> {
    match addr.metadata() {
        Ok(md) if md.is_dir() => Ok(read_dir(addr).into_response()),
        Ok(md) if md.is_file() => Ok(read_file(addr, range).await.into_response()),
        // Ok(md) if md.is_symlink() => Ok(read_link(addr).into_response()),
        Ok(md) => todo!("handle other metadata type {:?} {:?}", md.file_type(), md.is_file()),
        Err(e) => Err(e),
    }
}

async fn read_file(addr: &PathBuf, range: &Option<TypedHeader<Range>>) -> impl IntoResponse {
    let file = File::open(addr).await.unwrap();
    let body = KnownSize::file(file).await.unwrap();
    let range = range.clone().map(|TypedHeader(range)| range);
    let mime = fs::service::content_type(addr);
    let mm = match mime {
        Some(mm) => format!(" ({})", mm),
        None => String::from(""),
    };
    println!("file: {}{}", addr.display(), mm);

    let content_type = mime.map(|ct| ContentType::from(ct.parse::<mime::Mime>().unwrap()));
    Ranged::with_headers(range, body, content_type)
}

fn read_dir(addr: &PathBuf) -> impl IntoResponse {
    println!("dir:  {}", addr.display());
    match fs::service::read_dir(addr) {
        Ok(children) => Json(ResponseDTO::success(children)),
        Err(_) => todo!(),
    }
}

// fn read_link(addr: &PathBuf) -> impl IntoResponse {
//     println!("link: {}", addr.display());
//     std::fs::read_link(addr)
// }
//#endregion A

//#region upload path

/// path is a dir => create new file
/// path is a file => overwrite that file
/// path is a symlink => overwrite file
/// path is non-existed => create folder at the path and upload file
pub(crate) async fn upload_path(addr: &PathBuf, request: Request) -> Result<()> {
    // match addr.metadata() {
    // Ok(md) if md.is_dir() => Ok(write_to_dir(addr).into_response()),
    // Ok(md) if md.is_file() => Ok(write_to_file(addr, _range).await.into_response()),
    // Ok(md) if md.is_symlink() => Ok(write_to_link(addr).into_response()),
    // Ok(md) => todo!("handle other metadata type {:?} {:?}", md.file_type(), md.is_file()),
    // Err(e) => Err::<()>(e),
    // };

    // println!("path {}, is folder = {}", addr.display(), path.ends_with("/"));
    // let stream = request.into_body().into_data_stream();
    // stream_to_file(&addr, stream).await
    Ok(())
}

pub(crate) async fn upload_file(addr: &PathBuf, mut multipart: Multipart) -> Result<()> {
    println!("{}", addr.display());
    while let Some(field) = multipart.next_field().await.unwrap() {
        let name = field.name().unwrap().to_string();
        let file_name = field.file_name().unwrap();
        // let data = field.into_stream();
        // let stream_to_file = stream_to_file(&addr, data).await;
        println!("[{}] saving `{}` to {}", name, &file_name, addr.display());
    }
    Ok(())
}

async fn stream_to_file<S, E>(path: &PathBuf, stream: S) -> Result<()>
where
    S: Stream<Item = std::result::Result<bytes::Bytes, E>>,
    E: Into<axum::BoxError>,
{
    // Convert the stream into an `AsyncRead`.
    let body_with_io_error = stream.map_err(|err| Error::new(ErrorKind::Other, err));
    let body_reader = StreamReader::new(body_with_io_error);
    futures::pin_mut!(body_reader);

    // Create the file. `File` implements `AsyncWrite`.
    let mut file = BufWriter::new(File::create(path).await?);

    // Copy the body into the file.
    tokio::io::copy(&mut body_reader, &mut file).await?;

    Ok(())
}

// to prevent directory traversal attacks we ensure the path consists of exactly one normal component
fn path_is_valid(path: &PathBuf) -> bool {
    // let path = std::path::Path::new(path);
    let mut components = path.components().peekable();
    if let Some(first) = components.peek() {
        if !matches!(first, std::path::Component::Normal(_)) {
            return false;
        }
    }

    components.count() == 1
}
//#endregion upload path

//#region move path
pub(crate) async fn move_path(addr: &PathBuf) -> Result<String> {
    // tokio::fs::copy(from, to)
    // tokio::fs::rename(from, to)
    Ok(format!("{}", addr.display()))
}
//#endregion move path

//#region delete path
pub(crate) async fn delete_path(addr: &PathBuf) -> Result<()> {
    match addr.metadata() {
        Ok(md) if md.is_file() => tokio::fs::remove_file(&addr).await,
        Ok(md) if md.is_dir() => tokio::fs::remove_dir_all(&addr).await,
        Err(e) => Err(e),
        _ => todo!(),
    }
}
//#endregion delete path
