use serde::{Deserialize, Serialize};

// the input to our `create_user` handler
#[derive(Deserialize)]
pub(crate) struct CreateUser {
    pub(crate) username: String,
}

// the output to our `create_user` handler
#[derive(Serialize)]
pub(crate) struct User {
    pub(crate) id: u64,
    pub(crate) username: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ResponseDTO<T> {
    pub code: i32,

    pub message: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub body: Option<T>,
}

impl<T> ResponseDTO<T> {
    pub fn success(body: T) -> Self {
        ResponseDTO {
            code: 0,
            message: String::from("Success"),
            body: Some(body),
        }
    }

    pub fn error(message: String, body: T) -> Self {
        ResponseDTO {
            code: 1,
            message,
            body: Some(body),
        }
    }

    pub fn error_empty(message: String) -> Self {
        ResponseDTO {
            code: 1,
            message,
            body: None,
        }
    }
}
